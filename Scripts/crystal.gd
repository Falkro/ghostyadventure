extends TextureRect

export(String) var scene_to_load

func _on_death_area2_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene(str("res://Scene/Win.tscn"))

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene(str("res://Scene/Win.tscn"))
	pass # Replace with function body.
