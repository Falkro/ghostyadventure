extends Area2D

export(String) var scene_to_load

func _on_death_area2_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene(str("res://Scene/End.tscn"))
