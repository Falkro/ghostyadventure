extends LinkButton

export(String) var scene_to_load


func _on_New_Game_pressed():
	get_tree().change_scene(str("res://Scene/" + scene_to_load + ".tscn"))

func _on_RETRY_pressed():
	_on_New_Game_pressed()


func _on_QUIT_pressed():
	get_tree().quit()
	pass # Replace with function body.
