extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -800

var is_jumping = false

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Sprite")	

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		is_jumping = true
		velocity.y = jump_speed
		animator.play("jump")
		
	if Input.is_action_pressed('ui_right'):
		animator.scale.x = 1
		velocity.x = speed
		animator.play("run")
	elif Input.is_action_pressed('ui_left'):
		animator.scale.x = -1
		velocity.x = -speed
		animator.play("run")
	else:
		velocity.x = 0
		if is_on_floor() and !is_jumping:
			animator.play("idle")
	
	if is_on_floor():
		is_jumping = false
		
	velocity = move_and_slide(velocity, UP)
	
func die():
	get_tree().change_scene(str("res://Scene/GameOver.tscn"))
	print("Dead")
	pass