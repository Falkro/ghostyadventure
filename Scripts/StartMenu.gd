extends Control

export(String) var scene_to_load

func _ready():
	$Pokemon.play()
	
func _on_New_Game_pressed():
	$Pokemon.stop()
	get_tree().change_scene(str("res://Scene/" + scene_to_load + ".tscn"))

func _on_Quit_pressed():
	$Pokemon.stop()
	get_tree().quit()
	pass # Replace with function body.



func _on_Play_pressed():
	_on_New_Game_pressed()
