extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$audio.play()

func _on_death_area_body_entered(body):
	if body.name == "Player":
		body.die()

